//these store the current transmitter positions/values

#define MAX 2000
#define TOP 1630
#define BOTTOM 1250

#define HI 3
#define MID 2
#define LO 1


#define MAX_CHANNEL 6

int mem[MAX_CHANNEL+1];

void setup()
{

	for( int channel=1; channel<=MAX_CHANNEL; channel++){
		pinMode (input(channel), INPUT);
		pinMode (output(channel), OUTPUT);
		digitalWrite (output(channel), LOW);
		mem[channel] = LO;
	}


	Serial.begin(9600);
	Serial.println("Done with setup");
	for(int i=0; i<3;i++){
		digitalWrite(13, HIGH);   
	  	delay(200);             
	  	digitalWrite(13, LOW);   
	  	delay(200);  
	}
}

int state(unsigned long reading){
	if(reading > MAX) return LO;
	else if(reading > TOP) return HI;
	else if (reading < BOTTOM) return LO;
	else return MID;
}

int input(int channel){
	return channel*2;
}

int output(int channel){
	return input(channel)+1;
}

int readChannel(int channel){
	return state(pulseIn(input(channel), HIGH)); //read RC channel, wait max of 0.1 seconds for pulse
}

void writeChannel(int channel, int state){
	if(mem[channel] != state){

		Serial.print(channel);
		Serial.print(" ");
		Serial.println(state);

		mem[channel] = state;
		if(state == HI) digitalWrite(output(channel), HIGH);
		else digitalWrite(output(channel), LOW);
	}
}

void update(int channel){

	int state = readChannel(channel);

	

	

	
	writeChannel(channel,state);
}

void loop()
{
	for( int channel=1; channel<=MAX_CHANNEL; channel++){
		update(channel);
	}

	delay (800);
}
